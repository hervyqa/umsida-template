# Umsida Template

Template skripsi Universitas Muhammadiyah Sidoarjo. [Unduh versi terbaru disini](https://gitlab.com/hervyqa/umsida-template/-/releases).

## Unduh dan Pasang Libreoffice

Unduh perangkat lunak perkantoran LibreOffice di [libreoffice.org/download](https://www.libreoffice.org/download/download/)

## Memasang Amiri Fonts

Install berkas Amiri ttf (install all) yang ada di direktori fonts.

## Pengaturan LibreOffice

### Mengatur Metadata

Menu `Tools > Options > LibreOffice > User Data`. Isi data penunjang kepemilikan dokumen.

### Mengatur Locale

Menu `Tools > Options > Language Settings > Languages`.

- User interface: Default - English (USA)
- Locale setting: Indonesian
- Complex text layout: Saudi Arabia (Opsional)

### Mengatur Pengaturan Unit

Menu `Tools > Options > LibreOffice Writer > General`.

- Measurement unit: Centimeter

### Mengatur AutoCaption

Menu `Tools > Options > LibreOffice Writer > AutoCaption`. Tujuannya untuk membuat caption otomatis saat memasukkan gambar atau tabel.

- Ceklis LibreOffice Writer Table: Category `Tabel`
- Position: Above
- Ceklis LibreOffice Writer Image: Category `Gambar`
- Position: Below

### Menambahkan Database ODB

- Copy direktori (folder) `database`.
- (Windows) Paste dan replace ke `Local Disk C:\Users\Nama User\Appdata\Roaming\Libreoffice\4\user\`
- (Linux) Paste dan replace ke `~/.config/libreoffice/4/user/`

Tutup dan buka kembali libreofficenya. Persiapan selesai.

---

Credits:

- Amiri Fonts: [fonts.google.com/specimen/Amiri](https://fonts.google.com/specimen/Amiri)
